# requirements

    python2
    python-ldap
    bcrypt
    docopt

# description

this script creates a new base dn including organizational units for users, groups, services and the dn root user.

# usage

    ./slap.py -h
